# Introduction

This is an enhanced version of the 'lasker' internet chess server.
Andrew Tridgell (tridge@chess.samba.org) started to enhance Lasker
when he needed a working chess server for a local school. You can
get the [original 'lasker' release](http://chessd.sourceforge.net)
and also [Andrew Tridgell's enhanced version](http://chess.samba.org)


# Installation

First you need to configure and compile the chessd server. Do
something like the following:

``` bash
apt install build-essential gcc-multilib g++-multilib telnet
cd src
./configure --prefix=/var/games
make
make install
```

That will install a basic skeleton installation in /var/games/chessd


# Setting up

Next you can test your chess server using the commands:

``` bash
cd /var/games/chessd
./bin/chessd -f -p 5000
```

This will launch the chess server using the skeleton data you installed above.

Log in and set up the admin account:

``` bash
telnet localhost 5000
```

Use the login password 'admin', then press Enter. In the telnet console:

``` bash
addplayer admin [email address] [Your full name or nick]
asetpasswd admin [password]
```

You now have an administrator account. Don't use a password that you use
for any other system because these passwords will be sent in the clear
and anyone with Wireshark can potentially obtain it.

For better security (so that your chess server can't easily be taken over)
you may want to make a secondary account to use as a player.

``` bash
addplayer [username] [email address] [Your full name or nick]
asetpasswd [username] [password]
quit
```

To test with Xboard installed:

``` bash
echo -e 'username\npassword' > ~/.icsrc
xboard -ics -icshost localhost
```

If that seems to work then you can create a systemd daemon. Make a user
for the daemon first with:

``` bash
groupadd chess
useradd -c "FICS Chess Server" -d /var/games/chessd -m -r -g chess chess
```

Then create a daemon file such as */etc/systemd/system/chess-server.service*

``` text
[Unit]
Description=FICS Chess Server
After=network.target

[Service]
Type=simple
User=chess
Group=chess
WorkingDirectory=/var/games/chessd
ExecStart=/var/games/chessd/bin/chessd -f -p 5000
Restart=on-failure
RestartSec=10
PrivateTmp=true
PrivateDevices=false
NoNewPrivileges=true
CapabilityBoundingSet=~CAP_SYS_ADMIN
CPUQuota=30%

[Install]
WantedBy=multi-user.target
```

Activate the daemon with:

``` bash
systemctl enable chess-server
systemctl start chess-server
```

# Email spool

This chess server does not send emails directly, instead it puts
outgoing emails in the spool/ directory and waits for some external
program or script to deliver them. I designed it this way as it makes
it possible to send email from a chess server in a chroot jail, and
offers more flexibility in how email is handled (as that tends to vary
a lot between systems).

If you run sendmail then the sample script in scripts/spool_sendmail
might be good enough. Just run this script at startup and it will send
all spooled mail every minute.


# Server reload

This version of chessd supports reloading the server code without
having to shutdown the server or disturb any games that are in
progress. This allows for on the fly upgrades, hopefully without the
users even noticing that the system is being upgraded.

In order to support this functionality I had to make the source code
rather more Linux specific than it was previously, but I think that is
worth it for the functionality. It would be possible to port the code
to many other platforms, but I have not yet done so.

To reload the server use the command 'areload'. You must be at the
ADMIN_GOD admin level to issue this command.
