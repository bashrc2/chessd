/*
   Copyright (c) 1993 Richard V. Nash.
   Copyright (c) 2000 Dan Papasian
   Copyright (C) Andrew Tridgell 2002

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef FICSMAIN_H
#define FICSMAIN_H

#define MESS_WELCOME "welcome"
#define MESS_LOGIN "login"
#define MESS_LOGOUT "logout"
#define MESS_MOTD "motd"
#define MESS_ADMOTD "admotd"
#define MESS_UNREGISTERED "unregistered"

#define STATS_MESSAGES "messages"
#define STATS_LOGONS "logons"
#define STATS_GAMES "games"
#define STATS_JOURNAL "journal"

#endif /* FICSMAIN_H */
